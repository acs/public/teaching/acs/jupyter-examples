function out = cpl(t,x,u,p,opz)

if opz == 1
    out = x(2);
else
    out(1) = (u(1)-p(1)*x(1)-x(2))/p(2);
    out(2) = (x(1)-p(4)/x(2))/p(3);
end

end
    
