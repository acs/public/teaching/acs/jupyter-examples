function out = cpl2(t,x,u,p,opz)

if opz == 1
    out = x(2);
else
    iload = p(4)/x(2);
    
    if iload > p(5)
        iload = p(5);
    end
        
    out(1) = (u(1)-p(1)*x(1)-x(2))/p(2);
    out(2) = (x(1)-iload)/p(3);
end

end
    
