classdef CCVS < RCComponent
    properties
        cpos  
        cneg
        gain
    end
    methods
        function v = CCVS(p,n,cp,cn,g)
            v = v@RCComponent(p,n);
            v.gain = g;
            v.cpos = cp;
            v.cneg = cn;
           
        end   
        
        function ApplyMatrixStamp(v,P,dt)
            ntot = size(P.G,1);
            v.possource = ntot+1;
            P.G =  [P.G,zeros(ntot,2)];
            P.G =  [P.G; zeros(2,ntot+2)];
            P.b =  [P.b; 0];  
            if (v.Pos > 0)
                P.G(v.Pos, v.possource) = 1;  
                P.G(v.possource, v.Pos) = 1;               
            end
            if (v.Neg > 0)    
                P.G(v.Neg,v.possource) = -1;
                P.G(v.possource,v.Neg) = -1;
            end
            if(v.cpos > 0)
                    P.G(v.possource+1,v.cpos)=+1;
                    P.G(v.cpos,v.possource+1)=+1;
            end
            if(v.cneg > 0)
                    P.G(v.possource+1,v.cneg)=-1;
                    P.G(v.cneg,v.possource+1)=-1;
            end
            P.G(v.possource,v.possource+1) = v.g;         
        end
        
        function Init(v,P,dt)
            v.ApplyMatrixStamp(P,dt);         
        end
        
        function Step(v,P,dt,t)
        end
        
        function PostStep(v,vsol,dt)
        end

    end
end
            
            
            
        
