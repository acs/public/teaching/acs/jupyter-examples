classdef Inductor < RCComponent
    properties
        L  
        Gl
        Bl
        Il
        Vl
    end
    methods
        function l = Inductor(p,n,par)
            l = l@RCComponent(p,n);
            l.L = par;
        end
        
        function ApplyMatrixStamp(l,P,dt)
            l.Gl = dt/(2*l.L);
            if(l.Pos > 0)
                    P.G(l.Pos,l.Pos)=P.G(l.Pos,l.Pos)+l.Gl;
            end
            if(l.Neg > 0)
                    P.G(l.Neg,l.Neg)=P.G(l.Neg,l.Neg)+l.Gl;
            end
            if (l.Pos>0) & (l.Neg>0)
                P.G(l.Pos,l.Neg)=P.G(l.Pos,l.Neg)-l.Gl;
                P.G(l.Neg,l.Pos)=P.G(l.Neg,l.Pos)-l.Gl;
            end       
        end
        
        function ApplyMatrixStampdp(l,P,dt,om)
            l.Gl = 1/((2/dt+1j*om)*l.L);
            if(l.Pos > 0)
                    P.G(l.Pos,l.Pos)=P.G(l.Pos,l.Pos)+l.Gl;
            end
            if(l.Neg > 0)
                    P.G(l.Neg,l.Neg)=P.G(l.Neg,l.Neg)+l.Gl;
            end
            if (l.Pos>0) & (l.Neg>0)
                P.G(l.Pos,l.Neg)=P.G(l.Pos,l.Neg)-l.Gl;
                P.G(l.Neg,l.Pos)=P.G(l.Neg,l.Pos)-l.Gl;
            end       
        end
        
        function ApplyMatrixStampdpAdv(l,P,dt,om)
            a0 = 1;
            b0 = dt/2+1j*(1/om-dt/2*cot(om*dt/2));
            b1 = dt/2 - 1j*(1/om-dt/2*cot(om*dt/2));
            l.Gl = b1/((1+1j*om*b1)*l.L);
            if(l.Pos > 0)
                    P.G(l.Pos,l.Pos)=P.G(l.Pos,l.Pos)+l.Gl;
            end
            if(l.Neg > 0)
                    P.G(l.Neg,l.Neg)=P.G(l.Neg,l.Neg)+l.Gl;
            end
            if (l.Pos>0) & (l.Neg>0)
                P.G(l.Pos,l.Neg)=P.G(l.Pos,l.Neg)-l.Gl;
                P.G(l.Neg,l.Pos)=P.G(l.Neg,l.Pos)-l.Gl;
            end       
        end
        
        function Init(l,P,dt)
            l.ApplyMatrixStamp(P,dt);        
            l.Bl = 0;
            l.Vl = 0;
            l.Il = 0;
        end
        
        function Initdp(l,P,dt,om)
            l.ApplyMatrixStampdp(P,dt,om);        
            l.Bl = 0;
            l.Vl = 0;
            l.Il = 0;
        end
        
        function InitdpAdv(l,P,dt,om)
            l.ApplyMatrixStampdpAdv(P,dt,om);        
            l.Bl = 0;
            l.Vl = 0;
            l.Il = 0;
        end
        
        function Step(l,P,dt,t)
            l.Bl = l.Gl*l.Vl+l.Il;
            if (l.Pos > 0)
               P.b(l.Pos)= P.b(l.Pos)-l.Bl;
            end
             if (l.Neg > 0)
               P.b(l.Neg)= P.b(l.Neg)+l.Bl;          
            end
        end
        
        function Stepdp(l,P,dt,t,om)
            l.Bl = l.Gl*l.Vl+(2/dt-1j*om)/(2/dt+1j*om)*l.Il;
            if (l.Pos > 0)
               P.b(l.Pos)= P.b(l.Pos)-l.Bl;
            end
             if (l.Neg > 0)
               P.b(l.Neg)= P.b(l.Neg)+l.Bl;          
            end
        end
        
        function StepdpAdv(l,P,dt,t,om)
            a0 = 1;
            b0 = dt/2+1j*(1/om-dt/2*cot(om*dt/2));
            b1 = dt/2 - 1j*(1/om-dt/2*cot(om*dt/2));
            l.Bl = (b0*l.Vl+l.L*(a0-1j*om*b0)*l.Il)/(l.L*(1+1j*om*b1));
            if (l.Pos > 0)
               P.b(l.Pos)= P.b(l.Pos)-l.Bl;
            end
             if (l.Neg > 0)
               P.b(l.Neg)= P.b(l.Neg)+l.Bl;          
            end
        end
        
        function PostStep(l,vsol,dt)
            if (l.Pos > 0)
                if (l.Neg > 0)
                    l.Vl = vsol(l.Pos)-vsol(l.Neg);
                else
                    l.Vl = vsol(l.Pos);
                end
            else
                l.Vl = -vsol(l.Neg);
            end
                    
            l.Il = l.Gl*l.Vl+l.Bl;
        end
        
        function PostStepdp(l,vsol,dt,om)
            if (l.Pos > 0)
                if (l.Neg > 0)
                    l.Vl = vsol(l.Pos)-vsol(l.Neg);
                else
                    l.Vl = vsol(l.Pos);
                end
            else
                l.Vl = -vsol(l.Neg);
            end
                    
            l.Il = l.Gl*l.Vl+l.Bl;
        end
        
        function PostStepdpAdv(l,vsol,dt,om)
            if (l.Pos > 0)
                if (l.Neg > 0)
                    l.Vl = vsol(l.Pos)-vsol(l.Neg);
                else
                    l.Vl = vsol(l.Pos);
                end
            else
                l.Vl = -vsol(l.Neg);
            end
                    
            l.Il = l.Gl*l.Vl+l.Bl;
        end

    end
end
            
            
            
        
