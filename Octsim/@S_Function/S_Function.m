classdef S_Function < Block
    properties
        namefile
        p
        xo
    end
    methods
        function c = S_Function(in,out,name,xo,param)
            c = c@Block();
            c.namefile = name;
            c.ninput = size(in,1);
            c.noutput = size(out,1);
            c.inpos = in;
            c.outpos = out;
            c.nstate = size(c.xo,1);
            c.x = xo;
            c.xo = xo;
            c.p = param;
        end 
        
        function flag = Step(c,t,dt)
            command = sprintf("%s(t,c.x,c.u,c.p,1)",c.namefile);
            c.y = eval(command);
            c.updatestate(t,dt);      
            flag = 1;
        end
        
        function flag = Reset(b)
            b.x = b.xo;
            flag = 1;
        end
        
        function [dxdt] = cdxdt(c,x,t)
            command = sprintf("%s(t,c.x,c.u,c.p,2)",c.namefile);
            dxdt = eval(command);
        end
        
        
    end
end