classdef VoltageSensor < RCComponent
    properties
        volval
        flout
    end
    methods
        function b = VoltageSensor(p,n,flout)
            b = b@RCComponent(p,n);
            b.flout = flout;
            b.hyout = 1;
            b.volval = 0;
        end
        
        function PostStep(b,vsol,dt)
            if b.Pos > 0
                pos = vsol(b.Pos);
            else
                pos = 0;
            end    
            if b.Neg > 0
                neg = vsol(b.Neg);
            else
                neg = 0;
            end
                
            b.volval = pos-neg;
        end
        
        function flow = WriteFlowOut(b,flow)
            flow(b.flout) = b.volval;
        end
    end
end
            
            
            
        
