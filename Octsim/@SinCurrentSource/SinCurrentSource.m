classdef SinCurrentSource < RCComponent
    properties
        Amp
        om
        ph
    end
    methods
        function b = SinCurrentSource(p,n,M,O,P)
            b = b@RCComponent(p,n);
            b.Amp = M;
            b.om = O;
            b.ph = P;          
        end
        
        function ApplyMatrixStamp(b,P,dt)
            
        end
        
        function Init(b,P,dt)
            b.ApplyMatrixStamp(P,dt);         
        end
        
        function Step(b,P,dt,t)
         if (b.Pos > 0)
               P.b(b.Pos)= P.b(b.Pos)+b.Amp*sin(b.om*t+b.ph);
         end
        if (b.Neg > 0)
               P.b(b.Neg)= P.b(b.Neg)-b.Amp*sin(b.om*t+b.ph);          
        end
        
        end
        
        function PostStep(b,vsol,dt)
        end
    end
end
            
            
            
        
