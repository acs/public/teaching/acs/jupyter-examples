classdef Park < Block
    methods
        function c = Park(in1,in2,in3,out1,out2,out3,theta)
            c = c@Block();
            c.ninput = 4;
            c.noutput = 3;
            c.inpos(1) = in1;
            c.inpos(2) = in2;
            c.inpos(3) = in3;
            c.outpos(1) = out1;
            c.outpos(2) = out2;
            c.outpos(3) = out3;
            c.inpos(4) = theta;
         end 
        
        function flag = Step(c,t,dt)
            u = c.u;
            c.y(1) = sqrt(2/3)*(u(1)*cos(u(4))+u(2)*cos(u(4)-2*pi/3)+u(3)*cos(u(4)-4*pi/3));
            c.y(2) = sqrt(2/3)*(-u(1)*sin(u(4))-u(2)*sin(u(4)-2*pi/3)-u(3)*sin(u(4)-4*pi/3));
            c.y(3) = sqrt(2/3)*sqrt(2)/2*(u(1)+u(2)+u(3));
            flag = 1;
        end
                
    end
end
            
            
            
        
