classdef Switch < Block
    properties
           end
    methods
        function c = Switch(in1,in2,in3,out)
            c = c@Block();
            c.ninput = 3;
            c.noutput = 1;
            c.inpos(1) = in1;
            c.inpos(2) = in2;
            c.inpos(3) = in3;
            c.outpos(1) = out;
       end 
        
        function flag = Step(c,t,dt)
           if c.u(2) > 0
            c.y = c.u(1);
           else
            c.y = c.u(3);
          end

           flag = 1;
        end
        
        
    end
end
            
            
            
        
