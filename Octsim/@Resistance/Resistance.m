classdef Resistance < RCComponent
    properties
        R  
        Gc
    end
    methods
        function r = Resistance(p,n,par)
            r = r@RCComponent(p,n);
            r.R = par;
            if par > 0
                r.Gc = 1/par;
            else
               disp('Error: Resistance value not correct');
            end
            
        end   
        function ApplyMatrixStamp(r,P,dt)
            if(r.Pos > 0)
                    P.G(r.Pos,r.Pos)=P.G(r.Pos,r.Pos)+r.Gc;
            end
            if(r.Neg > 0)
                    P.G(r.Neg,r.Neg)=P.G(r.Neg,r.Neg)+r.Gc;
            end
            if (r.Pos>0) & (r.Neg>0)
                P.G(r.Pos,r.Neg)=P.G(r.Pos,r.Neg)-r.Gc;
                P.G(r.Neg,r.Pos)=P.G(r.Neg,r.Pos)-r.Gc;
            end       
        end
        
        function ApplyMatrixStampdp(r,P,dt,om)
            if(r.Pos > 0)
                    P.G(r.Pos,r.Pos)=P.G(r.Pos,r.Pos)+r.Gc;
            end
            if(r.Neg > 0)
                    P.G(r.Neg,r.Neg)=P.G(r.Neg,r.Neg)+r.Gc;
            end
            if (r.Pos>0) && (r.Neg>0)
                P.G(r.Pos,r.Neg)=P.G(r.Pos,r.Neg)-r.Gc;
                P.G(r.Neg,r.Pos)=P.G(r.Neg,r.Pos)-r.Gc;
            end       
        end
        
        function Init(r,P,dt)
            r.ApplyMatrixStamp(P,dt);         
        end
        
        function Initdp(r,P,dt,om)
            r.ApplyMatrixStampdp(P,dt,om);         
        end
        
        function InitdpAdv(r,P,dt,om)
            r.ApplyMatrixStampdp(P,dt,om);         
        end
        
        function Step(r,P,dt,t)
        end
        
        function Stepdp(r,P,dt,t,om)
        end
        
        function StepdpAdv(r,P,dt,t,om)
        end
        
        function PostStep(b,vsol,dt)
        end
        
        function PostStepdp(b,vsol,dt,om)
        end
        
        function PostStepdpAdv(b,vsol,dt,om)
        end

    end
end
            
            
            
        
