classdef Gain < Block
    properties
        k
    end
    methods
        function c = Gain(in,out,gain)
            c = c@Block();
            c.ninput = size(in,2);
            c.noutput = size(out,2);
            c.inpos = in;
            c.outpos = out;
           c.k = gain;
         end 
        
        function flag = Step(c,t,dt)
            c.y = c.k*c.u';
           flag = 1;
        end
        
        function flag = ChangeParameters(c,value)
            c.k = value;
            flag = 1;
        end
        
    end
end
            
            
            
        
