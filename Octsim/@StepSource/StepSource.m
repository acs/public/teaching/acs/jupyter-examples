classdef StepSource < Block
    properties
        startv
        endv
        ts
    end
    methods
        function c = StepSource(out,startv,endv,ts)
            c = c@Block();
            c.noutput = 1;
            c.outpos(1) = out;
            c.startv = startv;
            c.endv = endv;
            c.ts = ts;
            c.y = startv;
            
        end 
        
        function flag = Step(c,t,dt)
            if t>=c.ts
                c.y = c.endv;
            end
                
            flag = 1;
        end
        
    end
end
            
            
            
        
