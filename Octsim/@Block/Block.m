classdef Block < handle
    properties
        ninput;
        noutput;
        nstate;
        nparam;
        inpos;
        outpos;
        u;
        y;
        x;
        p;
	linear = 1; 
    end
    
    methods
        function b = Block()
            b.ninput=0;
            b.noutput=0;
            b.nstate = 0;
            b.nparam=0;
        end
        
        function flag = Init(b,initvalue)
            b.x = initvalue;
            flag = 1;
        end
        
        function flag = ChangeParameters(b,value)
        flag = 1;
        end
        
        function flag = Reset(b)
            b.x = zeros(b.nstate,1);
            flag = 1;
        end
        
        function flag = GetInput(b,sc)
            for i=1:b.ninput
                b.u(i)=sc.flows(b.inpos(i));
            end
            flag = 1;
        end
        
         function WriteOutput(b,sc)
            for i=1:b.noutput
                sc.flows(b.outpos(i))=b.y(i);
            end
            flag = 1;
        end
        
        function flag = Step(b,t,dt)
            for i=1:b.nstate
                b.x(i) = b.x(i);
            end
            flag = 1;
        end
        
        function [dxdt] = cdxdt(b,x,t)
            dxdt = 0;
        end
        
        function flag = updatestate(b,t,dt)
            dx1 = b.cdxdt(b.x,t);
            xp = b.x+dx1*dt;      
            dx2 = b.cdxdt(xp,t+dt);
            b.x = b.x+(dx1+dx2)/2*dt;
        end
            
    end
    
end
            
            
            
        
