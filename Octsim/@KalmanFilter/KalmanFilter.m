classdef KalmanFilter < DTBlock
    properties
        F
        G
        C
        Q
        R
        Pk
        nin
        nmeas
    end
    methods
        function c = KalmanFilter(in,out,nin,nmeas,T,F,G,C,Q,R,xo,Pk)
            c = c@DTBlock();
            c.F = F;
            c.G = G;
            c.C = C;
            c.Q = Q;
            c.R = R;
            c.x = xo;
            c.Pk = Pk;
            c.Ts = T;
            c.nin = nin;
            c.nmeas = nmeas;
            c.ninput = nin+nmeas;
            c.noutput = size(c.F,1);
            c.inpos = in;
            c.outpos = out;
            c.nstate = size(c.F,1);
        end 
        
        function flag=Reset(c)
            c.x = xo;
            c.Pk = Pk;
            flag = 1;
        end
            
        
        function updatediscrete(c)
            if (c.nin == 0)
             uu = 0;
             else
            for i=1:c.nin
               uu(i) = c.u(i);
            end
            endif
            for i=1:c.nmeas
               m(i) = c.u(c.nin+i);
            end
            c.x=c.F*c.x+c.G*uu;
            Pk = c.F*c.Pk*c.F'+c.Q;
            Kk = Pk*c.C'*inv(c.C*Pk*c.C'+c.R);
            c.x = c.x+Kk*(m-c.C*c.x);
            c.Pk= (eye(c.nstate)-Kk*c.C)*Pk;
            c.y = c.x;
           flag = 1;
        end
        
    end
end
            
            
            
        
