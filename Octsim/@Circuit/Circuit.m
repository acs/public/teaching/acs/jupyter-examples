classdef Circuit < handle
    
    properties 
        Pn
        Plin
        Pnl
        n
        NetList 
        nelements
    end
    
    methods
        function nl = Circuit(nnode)
           nl.n = nnode;
        end
        
        function AddComponent(nl,h)
            nl.nelements = size(nl.NetList,2)+1;
            nl.NetList{nl.nelements} = h;        
        end
        
        
        
        function Init(nl,dt)
            nl.Pn = LinProb(nl.n);
            nl.nelements = size(nl.NetList,2);
            for i=1:nl.nelements
                nl.NetList{i}.Init(nl.Pn,dt);
            end
            
        end
        
        function Initdp(nl,dt,om)
            nl.Pn = LinProb(nl.n);
            nl.nelements = size(nl.NetList,2);
            for i=1:nl.nelements
                nl.NetList{i}.Initdp(nl.Pn,dt,om);
            end
        end
        
        function InitdpAdv(nl,dt,om)
            nl.Pn = LinProb(nl.n);
            nl.nelements = size(nl.NetList,2);
            for i=1:nl.nelements
                nl.NetList{i}.InitdpAdv(nl.Pn,dt,om);
            end
        end
        
        function Initnl(nl,dt)
            nl.Pn = LinProb(nl.n);
            nl.nelements = size(nl.NetList,2);
            nl.Plin = LinProb(nl.n);
            
            for i=1:nl.nelements
                if (nl.NetList{i}.linear > 0)
                nl.NetList{i}.Init(nl.Plin,dt);
                end
            end
            nl.Pnl = LinProb(size(nl.Plin.G,1));
            for i=1:nl.nelements
                if (nl.NetList{i}.linear == 0)
                    nl.NetList{i}.Init(nl.Pnl,dt);
                end
            end
        end
        
        function vout = SolveDC(nl)
            for i=1:nl.nelements
                nl.NetList{i}.Step(nl.Pn,0);
            end    
            vout = nl.Pn.Solve();
        end
        
        
        function vout = SolveLinTran(nl,tini,tfin,dt)
            t = tini;
            k=1;
            while(t<= tfin)
                nl.Pn.Resetb();
                for i=1:nl.nelements
                    nl.NetList{i}.Step(nl.Pn,dt,t);
                end           
                vout(:,k) = nl.Pn.Solve();
                for i=1:nl.nelements
                    nl.NetList{i}.PostStep(vout(:,k),dt);
                end   
                t = t+dt;
                k=k+1;
            end
            
        end
        
        
        function vout = SolveLindpTran(nl,tini,tfin,dt,om)
            t = tini;
            k=1;
            while(t<= tfin)
                nl.Pn.Resetb();
                for i=1:nl.nelements
                    nl.NetList{i}.Stepdp(nl.Pn,dt,t,om);
                end           
                vout(:,k) = nl.Pn.Solve();
                for i=1:nl.nelements
                    nl.NetList{i}.PostStepdp(vout(:,k),dt,om);
                end   
                t = t+dt;
                k=k+1;
            end
            
        end
        
        function vout = SolveLindpAdvTran(nl,tini,tfin,dt,om)
            t = tini;
            k=1;
            while(t<= tfin)
                nl.Pn.Resetb();
                for i=1:nl.nelements
                    nl.NetList{i}.StepdpAdv(nl.Pn,dt,t,om);
                end           
                vout(:,k) = nl.Pn.Solve();
                for i=1:nl.nelements
                    nl.NetList{i}.PostStepdpAdv(vout(:,k),dt,om);
                end   
                t = t+dt;
                k=k+1;
            end
            
        end
        
        
           function vout = SolveNonLinTran(nl,tini,tfin,dt,maxn,toll)
            t = tini;
            k=1;
            vkold = zeros(GetSize(nl.Plin),1);
            while(t<= tfin)
                nl.Plin.Resetb();
                for i=1:nl.nelements
                    if (nl.NetList{i}.linear > 0)
                        nl.NetList{i}.Step(nl.Plin,dt,t);
                    end 
                end
                iter =1;
                err = 1e5;
                
                while iter<=maxn && (err>=toll)
                    nl.Pnl = LinProb(size(nl.Plin.G,1));
                    for i=1:nl.nelements
                    if (nl.NetList{i}.linear == 0)
                        nl.NetList{i}.MicroStep(nl.Pnl,dt,t,vkold);
                    end 
                    end
                    
                    nl.Pn = nl.Plin+nl.Pnl;
                
                    vknew = nl.Pn.Solve();
          
                    
                    delta = vknew-vkold;
                    err = delta'*delta;
                    vkold = vknew;
                    iter = iter+1;
                end
                
                vout(:,k) = vknew;
                
                for i=1:nl.nelements
                    nl.NetList{i}.PostStep(vout(:,k),dt);
                end   
                
                t = t+dt;
                k=k+1;
            end
            
        end
    end

    
end
            
            
            