classdef PI < Block
    properties
        A
        B
        C
        D
        ini
    end
    methods
        function c = PI(in,out,Kp,Ki,ini)
            c = c@Block();
            c.A = 0;
            c.B = 1;
            c.C = Ki;
            c.D = Kp;
            c.ninput = 1;
            c.noutput = 1;
            c.inpos(1) = in;
            c.outpos(1) = out;
            c.nstate = 1;
            c.x = ini;
            c.ini = ini;
         end 
        
     function flag = Step(c,t,dt)
            c.y = c.C*c.x+c.D*c.u;
            c.updatestate(t,dt);      
            flag = 1;
        end
        
        function flag = Reset(c)
            c.x = c.ini;
            flag = 1;
        end
        
        function flag = ChangeParameters(c,value)
           Kp = value(1);
           Ki = value(2);
           c.A = 0;
           c.B = 1;
           c.C = Ki;
           c.D = Kp;
           flag = 1;
        end
        
        function [dxdt] = cdxdt(c,x,t)
            dxdt = c.A*x+c.B*c.u;
        end
        
        
    end
end
            
            
            
        
