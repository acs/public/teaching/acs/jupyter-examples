classdef DTBlock < Block
    properties
        Ts;
        lastt = -10;
	linear = 1; 
    end
    
    methods
        function b = DTBlock()
            b=b@Block();
        end
        
        function flag = Step(b,t,dt)
            if t-b.lastt>=b.Ts
               b.updatediscrete();
               b.lastt = t;
            end
            flag = 1;
        end

        function updatediscrete(b)
        end
        
        function Reset(b)
            b.lastt = -10;
        end
    
    
    end
    
end
            
            
            
        
