classdef Schema < handle
    
    properties 
        nmodels;
        tini;
        tfin;
        flows;
        ModelList;
        t;
        dt;
        nflows;
    end
    
    methods
        function sc = Schema(ti,tf,dt,numflows)
           sc.tini = ti;
           sc.tfin = tf;    
           sc.nmodels = 0;
           sc.nflows = numflows;
           sc.dt = dt;
        end
        
        function AddComponent(sc,h)
            sc.nmodels = size(sc.ModelList,2)+1;
            sc.ModelList{sc.nmodels} = h;        
        end
        
        function AddListComponents(sc,l)
            p = size(l,2);
            sc.nmodels = size(sc.ModelList,2);
            for i=1:p
                sc.AddComponent(l{i});
            end             
        end
           
        function Init(sc)
            sc.t = sc.tini;
            for i=1:sc.nflows
                sc.flows(i) = 0;
            end
            
        end
        
        function Reset(sc)
            for i=1:sc.nmodels
                sc.ModelList{i}.Reset();
            end
        end
            
        function out = Run(sc,listout)
            sc.Init();
            qmax = size(listout,2);
            
            p=1;
            while sc.Step()
                out(1,p) = sc.GetTime();
                for q=1:qmax
                    out(1+q,p) = sc.GetFlow(listout(q));
                end
                p=p+1;
            end        
        end
        
        function flag = ChangeTimeStep(sc,dt)
            sc.dt = dt;
            flag = 1;
        end
            
        function flag = ChangeParameters(sc,pos,val)
            sc.ModelList{pos}.ChangeParameters(val);
            flag = 1;
        end
        
        function flag = Step(sc)
            for i=1:sc.nmodels
                sc.ModelList{i}.GetInput(sc);
                sc.ModelList{i}.Step(sc.t,sc.dt);
                sc.ModelList{i}.WriteOutput(sc);
            end
            sc.t = sc.t+sc.dt;
            if (sc.t < sc.tfin)
                    flag = 1;
            else
                    flag = 0;
            end
        end
        
        function val = GetFlow(sc,n)
            val = sc.flows(n);
        end     
           
        function val = GetTime(sc)
            val = sc.t;
        end
        
    end

    
end
            