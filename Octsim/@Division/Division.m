classdef Division < Block
    properties
    end
    methods
        function c = Division(in1,in2,out)
            c = c@Block();
            c.ninput = 2;
            c.noutput = 1;
            c.inpos(1) = in1;
            c.inpos(2) = in2;
            c.outpos(1) = out;
        end 
        
        function flag = Step(c,t,dt)
            c.y = c.u(1)/c.u(2);
           flag = 1;
        end
        
        
    end
end
            
            
            
        
