classdef DTDelay < DTBlock
    properties
        oldv
        ini
    end
    methods
        function c = DTDelay(in,out,ini,Ts)
            c = c@DTBlock();
            c.ninput = 1;
            c.noutput = 1;
            c.inpos(1) = in;
            c.outpos(1) = out;
	        c.Ts = Ts;
            c.oldv = ini;
        end 
        
       function flag = Reset(c)
            c.oldv = c.ini;
            flag =1;
        end
        
        function flag ChangeParameters(b,value)
            b.Ts = value;
            flag = 1;
        end
                    
       function updatediscrete(c)
          c.y = c.oldv;
          c.oldv = c.u;      
          flag = 1;
       end
        
    end
end
            
            
            
        
