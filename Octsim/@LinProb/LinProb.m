classdef LinProb < handle
    properties
        G
        b
    end
    
    methods
        function lp = LinProb(n)
            lp.G = zeros(n,n);
            lp.b = zeros(n,1);
        end
        
        function out = Solve(lp)
            out = lp.G\lp.b;
        end
        
        function Resetb(lp)
            lp.b = zeros(size(lp.b));
        end
        
        function C = plus(A,B)
            C = LinProb(A.GetSize());
            C.G = A.G+B.G;
            C.b = A.b+B.b;
        end
        
        function n = GetSize(lp)
            n = size(lp.G,1);
        end
                
    end
    
end
            
            
            
        
