classdef Fcnu < Block
    properties
        command
    end
    methods
        function c = Fcnu(in,out,comm)
            c = c@Block();
            c.ninput = size(in,2);
            c.noutput = size(out,2);
            c.inpos = in;
            c.outpos = out;
            c.command = comm;
         end 
        
        function flag = Step(c,t,dt)
            u = c.u;
            c.y = eval(c.command);
           flag = 1;
        end
               
    end
end
            
            
            
        
