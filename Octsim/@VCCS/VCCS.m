classdef VCCS < RCComponent
    properties
        cpos  
        cneg
        gain
    end
    methods
        function v = VCCS(p,n,cp,cn,g)
            v = v@RCComponent(p,n);
            v.gain = g;
            v.cpos = cp;
            v.cneg = cn;
           
        end   
        
        function ApplyMatrixStamp(v,P,dt)
            if(v.Pos > 0)
                if(v.cpos > 0)
                    P.G(v.Pos,v.cpos)=P.G(v.Pos,v.cpos)+v.gain;
                end
                if(v.cneg > 0)
                    P.G(v.Pos,v.cneg)=P.G(v.Pos,v.cneg)-v.gain;
                end
            end
            if(v.Neg > 0)
                if(v.cpos > 0)
                    P.G(v.Neg,v.cpos)=P.G(v.Neg,v.cpos)-v.gain;
                end
                if(v.cneg > 0)
                    P.G(v.Neg,v.cneg)=P.G(v.Neg,v.cneg)+v.gain;
                end
            end
        end
        
        function Init(v,P,dt)
            v.ApplyMatrixStamp(P,dt);         
        end
        
        function Step(v,P,dt,t)
        end
        
        function PostStep(v,vsol,dt)
        end

    end
end
            
            
            
        
