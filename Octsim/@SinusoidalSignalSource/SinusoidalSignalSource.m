classdef SinusoidalSignalSource < Block
    properties
        A
        om
        Ph
    end
    methods
        function c = SinusoidalSignalSource(out,Amp,omega,phase)
            c = c@Block();
            c.A = Amp;
            c.om = omega;
            c.Ph = phase;
            c.noutput = size(out,2);
            c.outpos = out;
        end 
        
        function flag = Step(c,t,dt)
            for i=1:c.noutput
                c.y(i) = c.A(i)*sin(c.om(i)*t+c.Ph(i)); 
            end
            flag = 1;
        end
        
    end
end
        
            
            
        
