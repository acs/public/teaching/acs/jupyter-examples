classdef Sum < Block
    properties
        s1
        s2
    end
    methods
        function c = Sum(in1,in2,out,sign1,sign2)
            c = c@Block();
            c.ninput = 2;
            c.noutput = 1;
            c.inpos(1) = in1;
            c.inpos(2) = in2;
            c.outpos(1) = out;
           c.s1 = sign1;
           c.s2 = sign2;
       end 
        
        function flag = Step(c,t,dt)
            c.y = c.s1*c.u(1)+c.s2*c.u(2);
           flag = 1;
        end
        
        
    end
end
            
            
            
        
