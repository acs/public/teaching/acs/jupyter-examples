classdef TransferFunction < Block
    properties
        A
        B
        C
        D
    end
    methods
        function c = TransferFunction(in,out,num,den)
            c = c@Block();
            c.ninput = 1;
            c.noutput = 1;
            c.inpos(1) = in;
            c.outpos(1) = out;
	        sys = tf(num,den);
            sysss= ss(sys);
            c.A = sysss.A;
            c.B = sysss.B;
            c.C = sysss.C;
            c.D = sysss.D;
            c.nstate = size(c.A,1);
            c.x = zeros(size(c.A,1),1);
        end 
        
        function flag = Step(c,t,dt)
            c.y = c.C*c.x+c.D*c.u;
            c.updatestate(t,dt);      
            flag = 1;
        end
        
        function [dxdt] = cdxdt(c,x,t)
            dxdt = c.A*x+c.B*c.u;
        end
        
        function flag = Reset(c)
            c.x = zeros(size(c.A,1),1);
            flag = 1;
        end
            
        
    end
end
            
            
            
        
