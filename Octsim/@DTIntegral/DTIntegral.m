classdef DTIntegral < DTBlock
    properties
    init
    end
    methods
        function c = DTIntegral(in,out,Ts,init)
            c = c@DTBlock();
            c.ninput = 1;
            c.noutput = 1;
            c.inpos(1) = in;
            c.outpos(1) = out;
	        c.Ts = Ts;
            c.nstate = 1;
            c.init = init;
            c.x = init;
        end 
        
       function flag = Reset(c)
           c.x = c.init;
           flag = 1
        end
       
        
       function updatediscrete(c)
          c.y = c.x;
          c.x=c.x+c.Ts*c.u';      
          flag = 1;
       end
        
    end
end
            
            
            
        
