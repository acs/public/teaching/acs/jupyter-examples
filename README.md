# Jupyter Example Profile

## Introduction

This repository contains an exemplary Jupyter profile which works with the RWTHjupyter cluster. To be more specific, it includes the following files

* `Quickstart.ipynb` which is an exemplary Jupyter notebook file.
* `environment.yml` which specifies the required Python packages needed to run `Quickstart.ipynb`. This file is used by Anaconda or `conda`.
* `Dockerfile` which defines the linux environment. In the end, the packages in `environment.yml` are installed.
* `.gitlab-ci.yml` which specifies the necessary Docker build commands (which are executed every time `Dockerfile` changes in Git).

## Contact

* In case of bugs and other cases, please contact [ACS-Teaching Team](acs-teaching-acs@eonerc.rwth-aachen.de).

The code is licensed under the [MIT license](https://opensource.org/licenses/MIT).
